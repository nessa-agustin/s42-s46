const mongoose = require('mongoose');

const orderSchema = {
    userId: {
        type: String,
        required: [true, "UserId is required"]
    },
    products: [
        {
            productId: {
                type: String,
                required: [true, "ProductId is required"]
            },
            quantity: {
                type: Number,
                default: 1
            }
        }
    ],
    totalAmount: {
        type: Number,
        required: [true, "Total is required"]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    shippingAddress: {
        type: String,
        required: [true, "Shipping Address is required"]
    },
    isCompleted: {
        type: Boolean,
        default: false
    }

}

module.exports = mongoose.model("Order",orderSchema)