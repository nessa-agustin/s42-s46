const UsersModel = require('../models/User')
const bcrypt = require('bcrypt');
const auth = require("../auth");


async function registerUser(req,res){

    const reqBody = req.body

    let newUser = new UsersModel(reqBody)
    newUser.password = bcrypt.hashSync(reqBody.password,10)

    try {
        let doc = await newUser.save();
        return res.send(true)

    } catch (error) {
        console.log(error)
        return res.send(false)
    }

}

 async function login(req,res){

    const reqBody = req.body
    let user = await UsersModel.findOne({email: reqBody.email})

    if(user){
        const checkPassword = bcrypt.compareSync(reqBody.password,user.password)
        if(checkPassword){
            return res.send({access: auth.createToken(user)})
        }
    }
        return res.send(false)

}

async function getUserDetails(req,res){

    const tokenData = await auth.decode(req.headers.authorization);
    const isAdmin = tokenData.isAdmin
    const authUserId = tokenData.id
    const userId = req.params.id
    const isUserValid = authUserId === userId || isAdmin

    if(isUserValid){

        try {
            const userDoc = await UsersModel.findById(userId,{password: 0})
            
            return res.send(userDoc);
            
        } catch (error) {
            console.log(error)
            return res.send(false)
        }
    }else{

        return res.status(401).send({message: 'You are not allowed to view this resource'})
    }

}

async function getAllUsers(req,res){

    const tokenData = await auth.decode(req.headers.authorization);
    const isAdmin = tokenData.isAdmin

    if(isAdmin){

        try {
            const userDoc = await UsersModel.find({},'-password')
            return res.send(userDoc);
            
        } catch (error) {
            console.log(error)
            return res.send(false)
        }
    }else{

        return res.status(401).send({message: 'You are not allowed to view this resource'})
    }

}

async function setAdmin(req,res){

    const tokenData = await auth.decode(req.headers.authorization);
    const isAuthAdmin = tokenData.isAdmin
    const userId = req.body.userId
    const isAdmin = req.body.isAdmin

    if(isAuthAdmin){

        try {
            const userDoc = await UsersModel.findByIdAndUpdate(userId,{isAdmin: isAdmin})
            return res.send(true);
            
        } catch (error) {
            console.log(error)
            return res.send(false)
        }
    }else{

        return res.status(401).send({message: 'You are not allowed to view this resource'})
    }

}

async function changePassword(req,res){

    const tokenData = await auth.decode(req.headers.authorization);
    const authUserId = tokenData.id

    let reqBody = req.body;

    try {

        let userDoc = await UsersModel.findById(authUserId);
        let isValidOldPw = bcrypt.compareSync(reqBody.oldPassword,userDoc.password);

        if(isValidOldPw){
            await UsersModel.findByIdAndUpdate(authUserId,{password: bcrypt.hashSync(reqBody.newPassword,10)});
            return res.send({result: true})
        }else{
            return res.status(401).send({error: 'Old Password not match'})
        }

    } catch (error) {
        console.log(error)
        return res.send({error: 'Change password failed',details: error})
    }
   

}

async function forgotPassword(req,res){

    let token = await auth.forgotPasswordToken(req.body.email)
    let resetLink = `${req.protocol}://${req.get('host')}/users/reset?token=${token}`

    return res.send({reset_link: resetLink})

}

async function resetPassword(req,res){

    const token = req.query.token;
    // console.log(token)

    const data = await auth.decode(token)

    try {
       await UsersModel.findOneAndUpdate(
            {email: data.email},
            {password: bcrypt.hashSync(req.body.password,10)}
            );

        return res.send({result: true})
    } catch (error) {
        console.log(error)
        return res.send({error: 'Reset Password Failed'})
    }

}



module.exports = {

    register: registerUser,
    login: login,
    getUserDetails: getUserDetails,
    getAllUsers: getAllUsers,
    changePassword: changePassword,
    forgotPassword: forgotPassword,
    resetPassword: resetPassword,
    setAdmin: setAdmin
}