const Products = require('../models/Product')
const auth = require('../auth');


async function addProduct(req,res){

    const tokenData = await auth.decode(req.headers.authorization);
    const isAdmin = tokenData.isAdmin;
    let reqBody = req.body;
    
    if(isAdmin){

        let newProduct = new Products(reqBody)


            return newProduct.save()
            .then((product) => {
                return res.send(true)
            })
            .catch(error => {
                console.log(error.errors)
                return res.send(false);
            })
    }else{
        res.status(401).send('You are not authorized to access this resource')
    }

}

async function getAllActiveProducts(req,res){

    let result = await Products.find({isActive: true});
    return res.send(result);
}

async function getAllProducts(req,res){

    const tokenData = await auth.decode(req.headers.authorization);
    const isAdmin = tokenData.isAdmin;

    if(isAdmin){
        let products = await Products.find({});
        return res.send(products);
    }else{
        res.status(401).send('You are not authorized to access this resource')
    }

    
}

async function getProduct(req,res){

    try {
        let product = await Products.findById(req.params.id);
        return res.send(product);
    } catch (error) {
        return res.status(404).send("Product not found.")
    }
  
}

async function updateProduct(req,res){

    let updateValues = req.body;
    const tokenData = await auth.decode(req.headers.authorization);
    const isAdmin = tokenData.isAdmin;

    if(isAdmin){
        try {
            let product = await Products.findByIdAndUpdate(
                req.params.id,
                updateValues,
                {
                    new: true
                }
            );
            return res.send(product);
            // return res.send(true);
    
        } catch (error) {
            // return error
            // return res.status(404).send("Product not found.")
            return false
        }
    }else{
        return res.status(401).send('You are not authorized to access this resource')
    }
    
  
}

async function archiveProduct(req,res){

    const tokenData = await auth.decode(req.headers.authorization);
    const isAdmin = tokenData.isAdmin;

    if(isAdmin){
        try {
            let product = await Products.findByIdAndUpdate(
                req.params.id,
                {isActive: false},
                {
                    new: true
                }
            );
            return res.send(product);
            // return res.send(true);
    
        } catch (error) {
            // return error
            // return res.status(404).send("Product not found.")
            return false
        }
    }else{
        return res.status(401).send('You are not authorized to access this resource')
    }
    
  
}


module.exports = {
    addProduct: addProduct,
    getAllActiveProducts: getAllActiveProducts,
    getAllProducts: getAllProducts,
    getProduct: getProduct,
    updateProduct: updateProduct,
    archiveProduct: archiveProduct
}